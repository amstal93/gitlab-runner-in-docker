FROM alpine
RUN apk add --update openssl
RUN wget -O /usr/local/bin/gitlab-runner https://gitlab-ci-multi-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-ci-multi-runner-linux-amd64
RUN chmod +x /usr/local/bin/gitlab-runner
RUN adduser -S -h gitlab-runner -s /bin/bash 'gitlab-runner'

RUN apk add ca-certificates

